﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAFE_Agente : ISSR_Agent{

	public ISSRState current_state; //Estado actual, para máquina de estados
	public ISSR_Object colliding_object; //Se usará al llamar a AvoidObstacle()

	public enum TEST_MsgCode
	{
		AvailableStone,
		NonAvailableStone
	}

	/* INICIO */
	public override void Start(){
		Debug.LogFormat("{0}   comienza", Myself.Name);
	}

	/* CORRUTINA */
	public override IEnumerator Update ()  {
		while (true) {
			yield return new WaitForSeconds(0.5f);

			current_event = ISSREventType.onTickElapsed;
			// Insertar código para esta situación
			current_state = AgentStateMachine();
		}
	}

	/* OBJETO QUE ENTRA EN NUESTRO CAMPO DE VISISON */
	public override void onEnterSensingArea (ISSR_Object obj){

		if (obj.type == ISSR_Type.SmallStone) // PIEDRA PEQUEÑA
		{
			bool available = (oiGrippingAgents (obj) == 0) ? true : false;

			if (available) {
				current_state = SStoneIsAvailable (obj, true);
			} else {
				current_state = SStoneIsNOTAvailable (obj, true);
			}
		}
	}




	/* PIEDRA AGARRADA CON EXITO */
	public override void onGripSuccess(ISSR_Object obj_gripped){
		Debug.LogFormat ("{0} agarra '{1}'", Myself.Name, obj_gripped.Name);
		acSendMsgObj(ISSRMsgCode.Assert, (int) TEST_MsgCode.NonAvailableStone, focus_object, Vector3.zero, 0, 0);
		current_state = AgentStateMachine ();
	}

	/* PIEDRA LLEVADA A LA META CON EXITO */
	public override void onGObjectScored(ISSR_Object stone_that_scored){
		Debug.LogFormat ("{0} piedra '{1}' metida en meta", Myself.Name, stone_that_scored.Name);
		current_state = AgentStateMachine (); // Llamar a máquina de estados
	}

	public override void onCollision(ISSR_Object obj_that_collided_with_me){
		colliding_object = obj_that_collided_with_me;
		current_state = AgentStateMachine ();

	}

	public override void onGObjectCollision(ISSR_Object obj_that_with_gripped_obj){
		colliding_object = obj_that_with_gripped_obj;
		current_state = AgentStateMachine ();
	}

	public virtual void onStop(){
	}

	public override void onDestArrived(){
		current_state = AgentStateMachine();
	}

	public override void onGripFailure(ISSR_Object obj_I_wanted_to_grip) {
		current_state = AgentStateMachine();
	}

	public override void onObjectLost(ISSR_Object obj_i_was_looking_for) {
		current_state = AgentStateMachine();
	}

	public void acSendMsg(ISSRMsgCode code, int usercode, Vector3 location=new Vector3(), float fvalue=0; int ivalue=0){

	}

	public void acSendMsgObj(ISSRMsgCode code, int usercode, ISSR_Object Obj,Vector3 location=new Vector3(), float fvalue=0; int ivalue=0){

	}

	void Share(){
		foreach ( ISSR_Object stone in Valid_Small_Stones) {
			// Informar de piedras que se creen disponibles
			acSendMsgObj(ISSRMsgCode.Assert, (int) TEST_MsgCode.AvailableStone, stone, Vector3.zero, 0, 0);
		}
		foreach ( ISSR_Object stone in Invalid_Small_Stones) {
			// Informar de piedras que se creen no disponibles
			acSendMsgObj(ISSRMsgCode.Assert, (int) TEST_MsgCode.NonAvailableStone, stone, Vector3.zero, 0, 0);
		}
		// En estos estados la piedra que agarro o busco la considero no disponible.
		if ((current_state == ISSRState.GoingToGoalWithSmallStone) || (current_state == ISSRState.WaitforNoStonesMoving)) {
			acSendMsgObj(ISSRMsgCode.Assert, (int) TEST_MsgCode.NonAvailableStone, focus_object, Vector3.zero, 0, 0);
		}
	}

	public override void onMsgArrived(ISSR_Message msg) {
		if (msg.usercode == (int) TEST_MsgCode.AvailableStone) {
			// Mensaje con piedra que se cree disponible
			current_state = SStoneIsAvailable (msg.Obj, false);
			current_state = AgentStateMachine();
		} else if (msg.usercode == (int) TEST_MsgCode.NonAvailableStone) {
			// Mensaje con piedra que se cree NO disponible
			current_state = SStoneIsNOTAvailable (msg.Obj, false);
			current_state = AgentStateMachine();
		}
	}

	ISSR_Object GetCloserObjectToGoal(List<ISSR_Object> ObjList, ISSR_Type obj_type)
	{
		Vector3 closest_location = new Vector3(0,0,0);
		float mindistance = Mathf.Infinity;
		float distance;
		Vector3 to_point;

		remaining_elements = ObjList.Count;

		if (remaining_elements > 0)
		{
			foreach( Vector3 location in LocList)
			{
				to_point = (location - a.oiLocation (a.Myself));
				distance = to_point.magnitude;

				if (distance < mindistance)
				{
					mindistance = distance;
					closest_location = location;
				}
			}
		}

		return closest_location;
	}

	/*-----------------------------------------------------------------------------------------------------------------------------------------------------*/

	/* ESTADO INACTIVO */
	ISSRState IdleState	(){

		ISSRState next_state = current_state;


		focus_object = ISSRHelp.GetCloserObjectInList (this, Valid_Small_Stones, ISSR_Type.SmallStone); // coger de lista objeto más cercano a agente que sea de tipo SmallStone, se convierte en el objeto de interés

		if (focus_object != null ){
			Debug.LogFormat ("{0} trato de coger '{1}'", Myself.Name, focus_object.Name);
			if (oiSensable (focus_object)){  // Si está a la vista
				Debug.LogFormat ("{0} '{1}' está cerca, pido cogerla", Myself.Name, focus_object.Name);
				acGripObject (focus_object);
				if (acCheckError ()){  // ERROR
					next_state = ISSRState.Idle;
				}
				else{
					next_state = ISSRState.GoingToGripSmallStone;
				}
			}
			else{
				Debug.LogFormat ("{0} '{1}' no la veo, voy a su última posición", Myself.Name, focus_object.Name);
				acGotoLocation(focus_object.LastLocation);
				next_state = ISSRState.GoingToGripSmallStone;
			}
		}
		else{
			Debug.LogFormat ("{0} no conozco más piedras pequeñas", Myself.Name);
		}
		return next_state;
	}

	/* ESTADO IR A POR PIEDRA PEQUEÑA */
	ISSRState GoingToGripSmallStone(){
		ISSRState next_state = current_state;
		if (current_event == ISSREventType.onGripSuccess){
			next_state = ISSRState.WaitforNoStonesMoving;
		}
		else if(current_event == ISSREventType.onCollision){
			next_state = AvoidObstacle(colliding_object);
		}
		return next_state;
	}

	/* ESTADO LLEVAR PIEDRA PEQUEÑA A LA META */
	ISSRState GoingToGoalWithSmallStone()
	{
		ISSRState next_state = current_state;

		if (current_event == ISSREventType.onGObjectScored){
			Valid_Small_Stones.Remove (focus_object);
			next_state = ISSRState.Idle;
		}
		else if(current_event == ISSREventType.onGObjectCollision){
			next_state = AvoidObstacle(colliding_object);

		}
		return next_state;
	}

	ISSRState WaitforNoStonesMoving(){

		ISSRState next_state = current_state;

		if(current_event == ISSREventType.onTickElapsed && iMovingStonesInMyTeam() == 0){
			acGotoLocation (iMyGoalLocation ());
			Debug.LogFormat ("{0} voy a meta con '{1}'", Myself.Name, focus_object.Name);
			next_state = ISSRState.GoingToGoalWithSmallStone;
		}

		if (current_event == ISSREventType.onCollision) {
			onUngrip (focus_object);
		}

		return next_state;
	}

	/* ESTADO EN COLISION */
	ISSRState AvoidObstacle(ISSR_Object colliding_object){  // Parámetro, el objeto que ha colisionado oc
		ISSRState next_state = current_state;
		Vector3 collider_direction; // dc dirección de colisión, a oc desde agente o piedra
		Vector3 collision;  // Vector perpendicular a anterior y dirección de avance
		Vector3 new_direction; // nd Dirección de movimiento para apartarse de oc
		//  da  es  oiAgentDirection (Myself)

		if (current_state != ISSRState.AvoidingObstacle)
		{ // Guardar estado anterior si no era ya 'Evitando obstáculo'
			last_state = current_state;
		}

		// Obtener dc Dirección hacia obstáculo, respecto a agente o piedra
		if (current_event == ISSREventType.onCollision)
		{ // Si es el agente el que ha chocado con el obstáculo
			// Vector desde el agente al objeto que ha colisionado
			collider_direction = oiLocation (colliding_object) - oiLocation (Myself);
		}
		else  // current_event == ISSREventType.onGObjectCollision
		{ // Si es el objeto agarrado por el agente el que ha chocado
			// Vector desde el objeto agarrado por el agente al objeto que ha colisionado
			collider_direction = oiLocation (colliding_object) - oiLocation (GrippedObject);
		}


		collision = Vector3.Cross (collider_direction, oiAgentDirection (Myself));
		// El signo del vector resultado es proporcional al seno del ángulo que forman:
		// Vector en dirección Y positivo si agente tiene que ir hacia la izquierda,
		// según su dirección de avance, negativo si tiene que ir a su derecha

		collision.Normalize ();  // Normalizarlo para que tenga longitud 1

		new_direction = Vector3.Cross (collision, collider_direction.normalized);
		// Dirección perpendicular a vector vertical y la dirección de colisión
		// será en la dirección de apartarse, tendrá longitud 1

		if (colliding_object.type == ISSR_Type.BigStone)
		{  // Si la piedra es grande me muevo dos unidades
			acGotoLocation (oiLocation (Myself)  + 2*new_direction.normalized);
			if (acCheckError()) { current_state = ISSRState.End;}
		}
		else
		{  // Si es un objeto pequeño como piedra pequeña, agente o meta
			// me muevo una unidad más una cantidad aleatoria entre 0 y 1
			acGotoLocation (oiLocation(Myself)  + (1 + Random.value)*new_direction.normalized);
			if (acCheckError()) { current_state = ISSRState.End;}
		}

		next_state = ISSRState.AvoidingObstacle; // Estado de salida

		return next_state;
	}

	ISSRState AvoidingObstacle(){
		ISSRState next_state = current_state;

		if (current_event == ISSREventType.onDestArrived) {
			next_state = ResumeAfterCollision ();
		}
		else if(current_event == ISSREventType.onGObjectCollision || current_event == ISSREventType.onCollision){
			next_state = AvoidObstacle(colliding_object);
		}
		return next_state;
	}


	/* ESTADO COLISION RESUELTA */
	ISSRState ResumeAfterCollision()
	{
		ISSRState next_state = current_state;

		switch (last_state)
		{  // Dependiendo del estado anterior a la colisión
		// Completamos las acciones pendientes...

		case ISSRState.GoingToGoalWithSmallStone:
			acGotoLocation (iMyGoalLocation ()); // Ir a meta
			if (acCheckError()) { current_state = ISSRState.End;}
			break;

		case ISSRState.GoingToGripSmallStone:
			if (oiSensable(focus_object))
			{ // Coger objeto de interés si está a la vista
				acGripObject (focus_object);
				if (acCheckError ()) // Comprobar errores
				{
					return ISSRState.End;
				}
			}
			else
			{ // Si no está a la vista ir a su última posición conocida
				acGotoLocation (focus_object.LastLocation);
				if (acCheckError ()) // Comprobar errores
				{
					return ISSRState.End;
				}
			}
			break;

		default: // Código para detectar estados para los que no tenemos comportamiento
			Debug.LogErrorFormat ("{0}@ResumeAfterCollision estado '{1}' no contemplado",
				Myself.Name, last_state);
			break;
		}
		next_state = last_state;
		return next_state;
	}

	ISSRState SStoneIsAvailable (ISSR_Object sstone, bool seen) {
		ISSRState next_state = current_state;

		if (seen) {
			if (!Valid_Small_Stones.Contains((sstone))) // NO LISTADA
			{
				Valid_Small_Stones.Add(sstone);
				Debug.LogFormat("{0} nueva piedra pequeña '{1}',la anoto", Myself.Name, sstone.Name);
			}

			if (Invalid_Small_Stones.Contains (sstone)) {
				Invalid_Small_Stones.Remove (sstone);
			}

			if (current_state == ISSRState.GoingToGripSmallStone){ // ESTABA DE CAMINO A UNA PIEDRA CONOCIDA PERO NO A LA VISTA

				if (sstone.Equals(focus_object)){  // ES MI PIEDRA
					acGripObject (focus_object);  // VOY A COGERLA

					if (acCheckError ()){ // ERROR
						current_state = ISSRState.Idle;
					}
				}
			}
		} else {
			// Solo anado una piedra que otro agente dice que esta disponible si no la conozco en ninguna de las dos listas.
			if ((!Invalid_Small_Stones.Contains (sstone)) && (!Valid_Small_Stones.Contains (sstone))) {
				Valid_Small_Stones.Add(sstone);
			}
		}
		return next_state;
	}

	ISSRState SStoneIsNOTAvailable (ISSR_Object sstone, bool seen) {
		ISSRState next_state = current_state;

		if (Valid_Small_Stones.Contains((sstone))) // NO LISTADA
		{
			Valid_Small_Stones.Remove(sstone);
		}

		if (!Invalid_Small_Stones.Contains (sstone)) {
			Invalid_Small_Stones.Add (sstone);
		}

		if (current_state == ISSRState.GoingToGripSmallStone){ // ESTABA DE CAMINO A UNA PIEDRA CONOCIDA PERO NO A LA VISTA

			if (sstone.Equals(focus_object)){  // ES MI PIEDRA
				acStop();

				if (acCheckError ()){ // ERROR
					current_state = ISSRState.End;
				}
				next_state = ISSRState.Idle;
			}
		}
		return next_state;
	}

	ISSRState GoingToGripSmallStoneState() {
		ISSRState next_state = current_state;

		switch (current_event) {
			case ISSREventType.onDestArrived:
				if (oiMoving (Myself)) {
					acStop ();
				}
				Valid_Small_Stones.Remove (focus_object);
				Invalid_Small_Stones.Add (focus_object);
				if (acCheckError ()) {
					current_state = ISSRState.End;
				}
				next_state = ISSRState.Idle;
				break;
			case ISSREventType.onGripFailure:
				if (oiMoving (Myself)) {
					acStop ();
				}
				Valid_Small_Stones.Remove (focus_object);
				Invalid_Small_Stones.Add (focus_object);
				if (acCheckError ()) {
					current_state = ISSRState.End;
				}
				next_state = ISSRState.Idle;
				break;
			case ISSREventType.onObjectLost:
				if (oiMoving (Myself)) {
					acStop ();
				}
				Valid_Small_Stones.Remove (focus_object);
				Invalid_Small_Stones.Add (focus_object);
				if (acCheckError ()) {
					current_state = ISSRState.End;
				}
				next_state = ISSRState.Idle;
				break;
			}
	}


	/* MAQUINA DE ESTADOS */
	ISSRState AgentStateMachine()
	{
		ISSRState next_state = current_state;

		switch(current_state){

			case ISSRState.Idle:   // INACTIVO
				next_state = IdleState ();
				if (acCheckError ()){
					return ISSRState.End;
				}
				break;

			case ISSRState.GoingToGripSmallStone: // IR A POR PIEDRA PEQUEÑA
				next_state = GoingToGripSmallStone();
				if (acCheckError ()){
					return ISSRState.End;
				}
				break;

			case ISSRState.AvoidingObstacle: // EN COLISION
				next_state = AvoidingObstacle();
				break;

			case ISSRState.WaitforNoStonesMoving:
				next_state = WaitforNoStonesMoving() ;
				break;


			case ISSRState.GoingToGoalWithSmallStone: // IR A META CON PIEDRA PEQUEÑA
				next_state = GoingToGoalWithSmallStone();
				if (acCheckError ()){
					return ISSRState.End;
				}
				break;

			case ISSRState.End:
				break;

			default:
				next_state = ResumeAfterCollision();
				break;
		}

		if (current_state != next_state){ // MENSAJE DE CAMBIO DE ESTADO
			Debug.LogFormat ("{0} Estado '{1}'-->'{2}' por evento '{3}'", Myself.Name, current_state, next_state, current_event);
		}

		return next_state;
	}
}
