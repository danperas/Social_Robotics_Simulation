﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CAFE_Equipo : ISSR_TeamBehaviour {
	public override void CreateTeam (){
		if  (! InitError()){ 
			if (RegisterTeam("CAFE", "Irish Coffe")){ 
				Debug.Log  ("EquipoCAFE    despierta");   
				for(int   i = 0; i < this.GetNumberOfAgentsInTeam();   i++){ 
					CreateAgent(new   CAFE_Agente());
				} 
			} 
		}        
	}
}
