﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// ----------------------------------------------------------------------------------------
//  File: ISSRDictState.cs
//   Coded by Enrique Rendón: enriqueblender@gmail.com  for ISSR Unity API
//  Summary:
//    Enumerator for representing a state of the agent
// ----------------------------------------------------------------------------------------

public enum ISSRState
{
	Idle,
	GoingToLocation,
	GoingToObject,
	GoingToGoal,
	GoingToGrip,
	GoingToGripSmallStone,
	GoingToGoalWithSmallStone,
	AvoidingObstacle,
	WaitforNoStonesMoving,
	GoingToGripBigStone,
	WaitingForHelpToMoveBigStone,
	GoingToGoalWithBigStone,
	Wandering,
	Scouting,
	GoingToMeetingPoint,
	SearchingForPartners,
	End
}